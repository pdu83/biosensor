/* ----------------------------------------------------------------
 *  Created: Aug. 8, 2017
 *
 *  Authors: Pengfei Du, Baskar Ganapathysubramanian
 *  Copyright (c) 2017 Baskar Ganapathysubramanian
 *  See accompanying LICENSE.
  ------------------------------------------------------------------------- */
/*
  Copyright 2016-2017 Baskar Ganapathysubramanian

  This file is part of TALYFem.

  TALYFem is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  TALYFem is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with TALYFem.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <talyfem/talyfem.h>
#include <math.h>
#include <fstream>
#include <string>
#include "lsqr.hpp"
#include "deviceInputData.hpp"
#include "deviceNodeData.hpp"
#include "deviceGridField.hpp"
#include "NSEquation.hpp"
#include "PNPEquation.hpp"

using namespace TALYFEMLIB;
using namespace EDD;
static char help[] =
    "Solves a coupled transient Navier-Stoke (NS) and Poisson-Nernst-Planck "
        "(PNP) problem!";

/*
  This code solves the transient ion distribution + fluid flow problem.
*/
int DDNodeData::nsd = 0;

int main(int argc, char **args) {
  PetscInitialize(&argc, &args, (char *) 0, help);
  try {
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    PetscMPIInt mpi_rank, mpi_size;
    MPI_Comm_size(PETSC_COMM_WORLD, &mpi_size);
    MPI_Comm_rank(PETSC_COMM_WORLD, &mpi_rank);
    // define the solver related parameters;
    DDInputData inputData;
    GRID *pGrid = NULL;
    {
      if ((argc == 2) && (strcmp(args[1], "--printParameters") == 0)) {
        if (rank == 0) {
          inputData.printAll(std::cout);
        }
        PetscFinalize();
        return -1;
      }
      // Read input data from file "config.txt"
      inputData.ReadFromFile();
      inputData.CFL();
      // Loging inputdata to std::clog or file
      if (rank == 0) std::clog << inputData;
      //  Check if inputdata is complete
      if (!inputData.CheckInputData()) {
        throw (std::string(
            "[ERR] Problem with input data, check the config file!"));
      }
      DDNodeData::nsd = inputData.nsd;
      DDNodeData::setSpecies(inputData);
      // Based on inputdata create Grid
      CreateGrid(pGrid, &inputData);
      if (!inputData.ifBoxGrid) pGrid->set_nsd(inputData.nsd);

      DDGridField data(&inputData);
      data.input_ = &inputData;
      // Construct gridfEqield based on Grid
      data.redimGrid(pGrid);
      // cluster mesh if required. This is not so neccessary now, but I decide
      // to keep it here.
      // if (inputData.beta_1 > 0)  data.meshclustering (&inputData);
      data.redimNodeData();
      data.SetIC(inputData.nsd);
      // Set Solver parameters
      bool do_accelerate = (inputData.use_elemental_assembly_ == 1);
      AssemblyMethod assembly_method = kAssembleGaussPoints;
      if (inputData.use_elemental_assembly_ == 1) {
        assembly_method = kAssembleElements;
      }
      // define navier-stokes eq.
      NSEquation nsEq(&inputData);
      nsEq.redimSolver(pGrid, inputData.nsd + 1, inputData.basisFunction, 0);
      nsEq.setData(&data);
      // define PNP eq.
      PNPEquation ddEq(&inputData, do_accelerate,
                       assembly_method);  // the equation solver
      ddEq.redimSolver(pGrid, 1 + inputData.num_species,
                       inputData.basisFunction,
                       0);  // Setting up solver for coupled DD Eq.
      ddEq.setData(&data);
      // The following is not used right now, but will be need when post
      // processing in the future
      // lsqr ddls (&inputData, assembly_method); // The least square solver.
      // This function findes nodal values from G.P values
      // ddls.redimSolver (pGrid, nOfGPs, inputData.basisFunction, 0); //
      // Setting up ksp solver for least square problem
      // ddls.setData (&data);
      PrintStatus("solver initialized!", rank);
      // print out the initial settings. This is good for early checking of the
      // problem settings.
      save_gf(&data, &inputData, "data_init.plt", "Initial Condition");
      bool notSteady = true;  // now is it not steady state; when stady, it is
      // set to be 'true';
      char resultfile[150];
      double error = 1.;  // L2 Error computed from results of current Exciton
      // and previous exciton
      double converge_number = 5e-5;
      double currenttime = 0;
      int i = 0;
      while (notSteady)  // loop for time steps
      {
        i++;
        error = 1.;
        int aa = 0;
        while (aa < 3)  // This loop is an interatiove solve of
          // diffusion-(neutral particle) and
          // drift-diffusion-(charged partical) equation
        {
          ddEq.Solve();  // Solving drift diffusion Equation (phi and ions
          // together)
          // nsEq.Solve (currenttime,inputData.dt*inputData.St);  //solving flow
          // problem;
          error = 0;
          double norm = 1;
          norm = sqrt(norm);
          error = data.relError(1, -1);
          PrintStatus(" condition error is = ", error);
          aa++;
        }
        // Check if test passed and print result
        PrintStatus("Checking solution...");
        if (inputData.ifPrintPltFiles && i % 1 == 0) {
          data.dimlize();
          sprintf(resultfile, "result.plt");
          save_gf(&data, &inputData, resultfile, 0.0);
          data.nondimlize();
        }
        inputData.currenttime += inputData.dt;
        data.update(currenttime);
        currenttime += inputData.dt;
        currenttime += inputData.dt;
        notSteady = i < 150;  // currenttime<inputData.endtime;  //reach steady
        // state? either to the end time(step), or steady
        // state
        PrintStatus("current time", currenttime, "   ", notSteady);
      }
    }
    cout << "The end of the program" << endl;
    DestroyGrid(pGrid);
  } catch (const std::string &s) {
    PetscFinalize();
    std::cerr << s << std::endl;
    return -1;
  } catch (std::bad_alloc e) {
    PetscFinalize();
    std::cerr << "Problem with memory allocation " << e.what();
    return -1;
  } catch (const TALYException &e) {
    e.print();
    PetscFinalize();
    return -1;
  }

  PetscFinalize();
  return 0;
}
