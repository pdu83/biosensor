/*
  Copyright 2016-2017 Baskar Ganapathysubramanian

  This file is part of TALYFem.

  TALYFem is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  TALYFem is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with TALYFem.  If not, see <http://www.gnu.org/licenses/>.
*/
// --- end license text --- //
#ifndef PNP_EQUATION_HPP
#define PNP_EQUATION_HPP

#include "deviceNodeData.hpp"
using namespace std;
namespace EDD {
MPITimer timers[4];  ///< for timing several parts of the code

// indices to timing array
const int kTimerSolve = 0;
const int kTimerAssemble = 1;
const int kTimerSNESSolve = 2;
const int kTimerUpdate = 3;

// need to track the first assemble and last update time
// see destructor for details.
double g_first_assemble;
double g_last_update;

/**
 * Class calculates and returns value of A and b matrix for drift diffusion
 * equations.
 * The boundary conditions are set up in PNPEquation::fillEssBC()
 * The non-linear solver SNES parameters are set in PNPEquation::Solve()
 * The solution is passed to GridField in each itteration in: UpdateGridField
 *
 */
class PNPEquation : public CEquation<DDNodeData> {
 public:
  int cont = 50;
  SNES snes;           ///< nonlinear solver context
  int counter;         ///< number of function calls
  DDInputData *idata;  ///< pointer to inputdata
  TezduyarUpwindFE PG_n;
  TezduyarUpwindFE PG_p;
  PNPEquation(DDInputData *idata, bool has_uniform_mesh = false,
              AssemblyMethod assembly_method = kAssembleGaussPoints);
  virtual ~PNPEquation();
  virtual void fillEssBC();
  virtual void Solve(double dt, double t);
  virtual void Integrands(const FEMElm &fe, ZeroMatrix<double> &Ae,
                          ZEROARRAY<double> &be);
};

PetscErrorCode UpdateGridField(Vec _xg, PNPEquation *ceqn, int flag = 0);
PetscErrorCode FormFunction(SNES snes, Vec x, Vec f, void *ceqn);
PetscErrorCode FormJacobian(SNES snes, Vec _xg, Mat jac, Mat B, void *ceqn_);

PNPEquation::PNPEquation(DDInputData *idata, bool has_uniform_mesh,
                         AssemblyMethod assembly_method)
    : CEquation<DDNodeData>(has_uniform_mesh, assembly_method), idata(idata) {
  counter = 0;
  timers[kTimerSolve].set_label("Solve");
  timers[kTimerAssemble].set_label("Assemble");
  timers[kTimerSNESSolve].set_label("SNESSolve");
  timers[kTimerUpdate].set_label("Update");
  g_first_assemble = 0.0;
  g_last_update = 0.0;
}

PNPEquation::~PNPEquation() {
  timers[kTimerSolve].PrintGlobalTotalSeconds();
  timers[kTimerAssemble].PrintGlobalTotalSeconds();
  double assemble_mod =
      (timers[kTimerAssemble].GetTotalTimeSeconds() - g_first_assemble) * -1.0;
  double update_mod =
      (timers[kTimerUpdate].GetTotalTimeSeconds() - g_last_update) * -1.0;
  timers[kTimerSNESSolve].AddToTotalTime(assemble_mod);
  timers[kTimerSNESSolve].AddToTotalTime(update_mod);
  timers[kTimerSNESSolve].PrintGlobalTotalSeconds();
  timers[kTimerUpdate].PrintGlobalTotalSeconds();
}

void PNPEquation::fillEssBC() {
  this->initEssBC();
  double Vt = idata->Vt;  // scale factor for phi (0.0256)

  for (int nodeID = 0; nodeID < p_grid_->n_nodes(); nodeID++) {
    DDNodeData &data = p_data_->GetNodeData(nodeID);
    double x = p_grid_->GetCoord(nodeID, 0);
    double y = p_grid_->GetCoord(nodeID, 1);
    // set the boundary where the concentration is fixed, e.g., inlet
    // condentration should be a constent number
    for (std::vector<int>::iterator it = idata->constC_BC.begin();
         it != idata->constC_BC.end(); ++it) {
      if (this->p_grid_->BoNode(nodeID, *it)) {  // interate through all
                                                 // species.
        for (int speciesID = 1; speciesID <= idata->num_species; ++speciesID) {
          this->specifyValue(nodeID, speciesID, 0.0);
          this->p_data_->GetNodeData(nodeID).ion[speciesID] = 0;
        }
      }
    }

    // set the applied voltage boundary
    for (std::vector<int>::iterator it = idata->potentialBC.begin();
         it != idata->potentialBC.end(); ++it) {
      if (this->p_grid_->BoNode(nodeID, *it)) {
        //     if(x>25&&x<26&&y<0.00001){
        this->specifyValue(nodeID, 0, 0.0);
        this->p_data_->GetNodeData(nodeID).phi[0] =
            idata->voltage / idata->V0();
        //   }
      }
    }
    // set the grounded surface to phi=0;
    for (std::vector<int>::iterator it = idata->groundBC.begin();
         it != idata->groundBC.end(); ++it) {
      if (this->p_grid_->BoNode(nodeID, *it)) {
        this->specifyValue(nodeID, 0, 0.0);
        this->p_data_->GetNodeData(nodeID).phi[0] = 0.;
      }
    }

    // the following only works for manufactured solution, for a real problem,
    // comment them
    if (this->p_grid_->BoNode(nodeID, 1) || this->p_grid_->BoNode(nodeID, 2) ||
        this->p_grid_->BoNode(nodeID, 3) || this->p_grid_->BoNode(nodeID, 4)) {
      this->specifyValue(nodeID, 0, 0.0);
      this->specifyValue(nodeID, 1, 0.0);
      this->specifyValue(nodeID, 2, 0.0);
      this->p_data_->GetNodeData(nodeID).phi[0] =
          PhiSolAt(idata->currenttime, x, y);
      this->p_data_->GetNodeData(nodeID).ion[1] =
          CnSolAt(idata->currenttime, x, y);
      this->p_data_->GetNodeData(nodeID).ion[0] =
          CpSolAt(idata->currenttime, x, y);
    }
  }
}

void PNPEquation::Solve(double dt = 0.0, double t = 0.0) {
  timers[kTimerSolve].Start();
  PetscErrorCode ierr;

  // register dirichlet boundary conditions
  this->fillEssBC();

  // Assemble the Jacobian matrix and Function vector
  timers[kTimerAssemble].Start();
  this->Assemble(false);
  timers[kTimerAssemble].Stop();

  g_first_assemble = timers[kTimerAssemble].GetLastTimeSeconds();

  // apply dirichlet boundary conditions
  this->ApplyEssBC();

  // Create nonlinear solver context
  ierr = SNESCreate(PETSC_COMM_WORLD, &snes);
  CHKERRABORT(PETSC_COMM_WORLD, ierr);

  // Set initial guess while checking if mesh
  // partitioning is used i.e. parallel_type_ == kWithDomainDecomp
  if (this->p_grid_->parallel_type_ == kWithDomainDecomp) {
    // array holds the local DOF values we want to put into the global
    // solution vector as our initial guess
    PetscScalar *array;
    ierr = PetscMalloc(this->n_total_dof() * sizeof(PetscScalar), &array);
    CHKERRABORT(PETSC_COMM_WORLD, ierr);

    // The global solution vector is stored as a distributed PETSc vector.
    // That means the storage for it is split up among all processors in the
    // system. Since the vector is distributed, we can't just write our local
    // DOF values to it when running in parallel.
    // However, PETSc has a "vector scatter" system that can be used to
    // "scatter" values from a local vector into a distributed vector.

    // So, we  create initVector (and use 'array' as the storage backing it).
    // initVector (aka 'array') will hold the DOF values stored on *this*
    // processor that we want to put into the global vector (this->xg_).
    // The mapping indices have been pre-computed by the CEquation base class
    // (which we inherit from) and are available as this->to() and this->from().
    Vec initVec;
    VecScatter scatter;
    ierr = VecCreateSeqWithArray(PETSC_COMM_SELF, 1, this->n_total_dof(), array,
                                 &initVec);
    CHKERRABORT(PETSC_COMM_WORLD, ierr);

    // fill 'array' with our local DOF values from p_data_ (the GridField)
    for (int nodeID = 0; nodeID < this->p_grid_->n_nodes(); nodeID++) {
      for (int i = 0; i < n_dof(); i++) {
        if (i == 0) {
          array[n_dof() * nodeID + i] =
              this->p_data_->GetNodeData(nodeID).phi[0];
        } else {
          array[n_dof() * nodeID + i] =
              this->p_data_->GetNodeData(nodeID).ion[i - 1];
        }
      }
    }

    // Use the PETSc scatter system to copy initVec into xg_
    ierr = VecScatterCreate(initVec, this->to(), this->xg_, this->from(),
                            &scatter);
    CHKERRABORT(PETSC_COMM_WORLD, ierr);
    ierr = VecScatterBegin(scatter, initVec, this->xg_, INSERT_VALUES,
                           SCATTER_FORWARD);
    CHKERRABORT(PETSC_COMM_WORLD, ierr);
    ierr = VecScatterEnd(scatter, initVec, this->xg_, INSERT_VALUES,
                         SCATTER_FORWARD);
    CHKERRABORT(PETSC_COMM_WORLD, ierr);

    // free everything
    ierr = VecDestroy(&initVec);
    CHKERRABORT(PETSC_COMM_WORLD, ierr);
    ierr = VecScatterDestroy(&scatter);
    CHKERRABORT(PETSC_COMM_WORLD, ierr);
    ierr = PetscFree(array);
    CHKERRABORT(PETSC_COMM_WORLD, ierr);
  } else {
    // when we aren't running in distributed mode,
    // we can just directly copy the values from p_data_
    // into xg_ with VecSetValues (in chunks of size n_dof())
    double *val = new double[this->n_dof()];
    PetscInt *index = new PetscInt[this->n_dof()];
    for (int nodeID = 0; nodeID < this->p_grid_->n_nodes(); nodeID++) {
      for (int i = 0; i < this->n_dof(); i++) {
        if (i == 0) {
          val[i] = this->p_data_->GetNodeData(nodeID).phi[0];
        } else {
          val[i] = this->p_data_->GetNodeData(nodeID).ion[i - 1];
        }

        index[i] = this->n_dof() * nodeID + i;
      }

      ierr = VecSetValues(this->xg_, this->n_dof(), index, val, INSERT_VALUES);
      CHKERRABORT(PETSC_COMM_WORLD, ierr);
    }
    delete[] val;
    delete[] index;
    ierr = VecAssemblyBegin(this->xg_);
    CHKERRABORT(PETSC_COMM_WORLD, ierr);
    ierr = VecAssemblyEnd(this->xg_);
    CHKERRABORT(PETSC_COMM_WORLD, ierr);
  }

  // Set function evaluation routine and vector.
  ierr = SNESSetFunction(snes, this->bg_, FormFunction, this);
  CHKERRABORT(PETSC_COMM_WORLD, ierr);

  // Set jacobian matrix
  ierr = SNESSetJacobian(snes, this->Ag_, this->Ag_, FormJacobian, this);
  CHKERRABORT(PETSC_COMM_WORLD, ierr);

  // Set non-linear solver tolerances
  double rtol = 1e-14;
  int maxit = cont, maxf = 100000;
  counter = 0;  // Resetting function counter
  SNESSetTolerances(snes, PETSC_DEFAULT, rtol, PETSC_DEFAULT, maxit, maxf);
  SNESSetFromOptions(snes);

  // Do the solve
  timers[kTimerSNESSolve].Start();
  ierr = SNESSolve(snes, PETSC_NULL, this->xg_);
  CHKERRABORT(PETSC_COMM_WORLD, ierr);
  timers[kTimerSNESSolve].Stop();

  // Get number of iterations used for logging
  PetscInt its;
  SNESGetIterationNumber(snes, &its);
  PetscPrintf(PETSC_COMM_WORLD, "number of iterations = %d\n", its);

  // Store solution to p_data_
  timers[kTimerUpdate].Start();
  UpdateGridField(this->xg_, this);
  timers[kTimerUpdate].Stop();
  g_last_update = timers[kTimerUpdate].GetLastTimeSeconds();

  // free the SNES object
  ierr = SNESDestroy(&snes);
  CHKERRABORT(PETSC_COMM_WORLD, ierr);

  timers[kTimerSolve].Stop();
}

/**
 * Returns the values of Ae[j,k] and be[j] for drift diffusion equations.
 * This code is written for solving 2 equations of drift diffusion.
 * The variables are named cation,anion.
 * By using linear basis one can access and find these 2 unknowns.
 * By using Hermite one can find these variables + their derivative + jn and jp
 * + grad of jn and jp in the wcation domain.
 */
void PNPEquation::Integrands(const FEMElm &fe, ZeroMatrix<double> &Ae,
                             ZEROARRAY<double> &be) {
  /***
  * General parameters of finite element system:
  */
  const int nsd =
      fe.nsd();  // number of spatial dimensions of system (1, 2, or 3)
  const int nbf = fe.nbf();
  const double detJxW = fe.detJxW();  ///< Jacobian of the system
  const int nndof = n_dof();
  /***
  * Computations regarding to previous iteration:
  */
  ZEROPTV
  dphi;  // derivative of "phi" from last iteration in x, y and z direction
  ZEROMATRIX<double> dion_pre(
      8, 2);  // derivative of "p" from last timestep in x, y and z direction
  ZEROMATRIX<double> dion(8, 2);  // derivatives from current timestep
  double theta = 1;               // idata->theta;
  double dt = idata->dt;
  ZEROPTV ion_pre;
  ZEROPTV ion;
  double phi = this->p_data_->valueFEM(fe, phi_id);
  ZEROPTV u;
  for (int speciesID = 0; speciesID < idata->num_species; speciesID++) {
    ion_pre(speciesID) = this->p_data_->valueFEM(fe, old_ion_id[speciesID]);
    ion(speciesID) = this->p_data_->valueFEM(fe, ion_id[speciesID]);
  }
  for (int i = 0; i < nsd; i++) {
    u(i) = 0 * idata->U_unhat(this->p_data_->valueFEM(fe, ux_id + i)) *
           idata->prefactorOfU;
    dphi(i) = this->p_data_->valueDerivativeFEM(fe, phi_id, i);
    for (int speciesID = 0; speciesID < idata->num_species; speciesID++) {
      // derivative of ion from last timestep
      dion_pre(speciesID, i) = this->p_data_->valueDerivativeFEM(
          fe, old_ion_id[speciesID],i);
      // derivative of ion from current timestep
      dion(speciesID, i) = this->p_data_->valueDerivativeFEM(
          fe, ion_id[speciesID], i);
    }
  }
  double muanion_dim =
      idata->mu_anion;  // dimensional form of mu_anion from config file
  double mucation_dim =
      idata->mu_cation;  // dimensional form of mu_cation from config file
  double eps_dim = this->p_data_->valueFEM(
      fe, eps_ID);  // dimensional form of epsilon from config file
  double lambda_2 = idata->lambda2_(eps_dim);  // Lambda sq.
  double gamma = 0;
  double RHSpotential = 0;
  for (int speciesID = 0; speciesID < idata->num_species; speciesID++) {
    RHSpotential -= ion(speciesID) * idata->valence[speciesID];
  }

  ZEROPTV U_n;       // Advection term for anion
  ZEROPTV U_p;       // Advection term for cation
  U_n(0) = dphi(0);  // x component term of advection for anion
  U_n(1) = dphi(1);  // y component term of advection for anion
  U_n(2) = 0;
  U_p(0) = -dphi(0);  // x component term of advection for ion
  U_p(1) = -dphi(1);  // y component term of advection for ion
  U_p(2) = 0;
  PG_n.calcSUPGWeightingFunction(fe, U_n, 1);
  PG_p.calcSUPGWeightingFunction(fe, U_p, 1);
  /***
  * Computations of non dimensional form of input parameters:
  */
  double mu_anion =
      idata->mu_hat(muanion_dim);  // non-dimensional form of mu_anion
  double mu_cation =
      idata->mu_hat(mucation_dim);  // non-dimensional form of mu_cation
  // set the source and sink term on the right hand side of species equation
  // (mainly due to chemical reaction)
  double source = 0.;
  double source_pre = 0.0;
  double sink = 0.;
  double sink_pre = 0.0;

  // test of manufactured solution, remove this if your are doing a real problem
  double x = p_grid_->GetCoord(fe.elem()->elm_id(), 0);
  double y = p_grid_->GetCoord(fe.elem()->elm_id(), 1);

  double source1[2], source1_pre[2], source2[2], souce2_pre[2];
  RHSpotential -= PhiForceAt(idata->currenttime, x, y);
  source1[1] = CnForceAt(idata->currenttime, x, y, 1);
  source1_pre[1] = CnForceAt(idata->currenttime - idata->dt, x, y, 1);
  source1[0] = CpForceAt(idata->currenttime, x, y, 1);
  source1_pre[0] = CpForceAt(idata->currenttime - idata->dt, x, y, 1);
  lambda_2 = 1;
  // Looping over basis functions
  for (int j = 0; j < nbf; j++) {    // Looping over basis functions
    for (int k = 0; k < nbf; k++) {  // Looping of over coupled variables (phi,
                                     // species_1, species_2,....species_N)
      for (int n_dofi = 0; n_dofi < nndof; n_dofi++) {
        for (int n_dofj = 0; n_dofj < nndof; n_dofj++) {
          if (n_dofi == 0) {    // poisson equation
            if (n_dofj == 0) {  // first column of poisson
              if (k == 0) {
                double dphi_dw = 0;
                for (int index = 0; index < nsd; index++) {
                  dphi_dw += dphi(index) * fe.dN(j, index);
                }
                be(nndof * j + n_dofi) +=
                    (2 * dphi_dw * lambda_2 + RHSpotential * fe.N(k)) * detJxW;
              }
              double N = 0.;
              for (int index = 0; index < nsd; index++) {
                N += fe.dN(j, index) * fe.dN(k, index);
              }
              Ae(nndof * j + n_dofi, nndof * k + n_dofj) +=
                  2 * (N * lambda_2) * detJxW;
            } else  // the rest are the derivatives with respect to each species
            {
              Ae(nndof * j + n_dofi, nndof * k + n_dofj) -=
                  idata->valence[n_dofj - 1] * fe.N(k) * detJxW;
            }
          }     // the end of poisson equation
          else  // loop over ion equaitons
          {
            int speciesID = n_dofi - 1;
            int valence = idata->valence[speciesID];
            if (n_dofj == 0) {
              double N = 0;
              for (int index = 0; index < nsd; index++) {
                N += fe.dN(j, index) * fe.dN(k, index);
              }
              Ae(nndof * j + n_dofi, nndof * k + n_dofj) =
                  valence * mu_anion * ion(speciesID) * N * detJxW * dt * theta;

              if (k == 0) {
                double dion_dN = 0;
                double dionpre_dN = 0;
                double mu_dphiPu_c = 0;
                double mu_dphiprePu_c = 0;
                for (int index = 0; index < nsd; index++) {
                  dion_dN += dion(speciesID, index) * fe.dN(j, index);
                  dionpre_dN += dion_pre(speciesID, index) * fe.dN(j, index);
                  mu_dphiPu_c +=
                      (mu_cation * dphi(index) * valence + u(index)) *
                      fe.dN(j, index);
                  mu_dphiprePu_c +=
                      (mu_cation * dphi(index) * valence + u(index)) *
                      fe.dN(j, index);
                }
                be(nndof * j + n_dofi) +=
                    ( (ion(speciesID) - ion_pre(speciesID)) * fe.N(j) +
                      dt * theta *
                         ((mu_cation * dion_dN + ion(speciesID) * mu_dphiPu_c) -
                          (source1[speciesID] - sink) * fe.N(j)) +
                      dt * (1. - theta) *
                         ((mu_cation * dionpre_dN +
                           ion_pre(speciesID) * mu_dphiprePu_c) -
                          (source1_pre[speciesID] - sink_pre) * fe.N(j))) * detJxW;
              }
            } else if (n_dofj == n_dofi) {
              double N = 0;
              double K_phi = 0;
              for (int index = 0; index < nsd; index++) {
                N += fe.dN(j, index) * fe.dN(k, index);
                K_phi = fe.dN(j, index) * fe.N(k) *
                        (mu_cation * dphi(index) * valence + u(index));
              }
              Ae(nndof * j + n_dofi, nndof * k + n_dofj) +=
                  (dt * theta * ((mu_cation * N + K_phi)) + fe.N(k)) * detJxW;
            } else {
            }
          }  // the end of ion equations
        }
      }
    }
  }
}

/***
 * Copies data from the global PETSc solution vector (_xg) into
 * ceqn->solution_, and then copies ceqn->solution_ into the
 * GridField (ceqn->p_data_).
 * @param _xg global solution vector
 * @param ceqn PNPEquation instance to pull grid values from
 */
PetscErrorCode UpdateGridField(Vec _xg, PNPEquation *ceqn, int flag) {
  PetscErrorCode ierr;
  if (ceqn->p_grid_->parallel_type_ == kWithDomainDecomp) {
    // Create a PETSc vector backed by ceqn->solution_.data() so we can
    // scatter from the global vector (_xg) into local ceqn->solution_ vector.
    VecScatter scatter;
    Vec SolutionVec;
    ierr = VecCreateSeqWithArray(PETSC_COMM_SELF, 1, ceqn->n_total_dof(),
                                 ceqn->solution_.data(), &SolutionVec);
    CHKERRQ(ierr);

    // do the scatter
    ierr =
        VecScatterCreate(_xg, ceqn->from(), SolutionVec, ceqn->to(), &scatter);
    CHKERRQ(ierr);
    ierr = VecScatterBegin(scatter, _xg, SolutionVec, INSERT_VALUES,
                           SCATTER_FORWARD);
    CHKERRQ(ierr);
    ierr = VecScatterEnd(scatter, _xg, SolutionVec, INSERT_VALUES,
                         SCATTER_FORWARD);
    CHKERRQ(ierr);

    // clean up
    ierr = VecDestroy(&SolutionVec);
    CHKERRQ(ierr);
    ierr = VecScatterDestroy(&scatter);
    CHKERRQ(ierr);
  } else {
    // send all of _xg to every processor into solution_vec
    // VecScatterCreateToAll also creates the solution_vec vector
    VecScatter scatter;
    Vec solution_vec;
    ierr = VecScatterCreateToAll(_xg, &scatter, &solution_vec);
    CHKERRQ(ierr);

    // do the scatter
    ierr = VecScatterBegin(scatter, _xg, solution_vec, INSERT_VALUES,
                           SCATTER_FORWARD);
    CHKERRQ(ierr);
    ierr = VecScatterEnd(scatter, _xg, solution_vec, INSERT_VALUES,
                         SCATTER_FORWARD);
    CHKERRQ(ierr);

    // copy the solution_vec vector (containing the new data)
    // into ceqn->solution_
    double *array;
    ierr = VecGetArray(solution_vec, &array);
    CHKERRQ(ierr);
    memcpy(ceqn->solution_.data(), array,
           sizeof(double) * (ceqn->n_total_dof()));
    ierr = VecRestoreArray(solution_vec, &array);
    CHKERRQ(ierr);

    // free the PETSc scatter and temporary vector
    ierr = VecScatterDestroy(&scatter);
    CHKERRQ(ierr);
    ierr = VecDestroy(&solution_vec);
    CHKERRQ(ierr);
  }
  if (ceqn->idata->isPeriodic) ceqn->updatePeriodicSol();
  // Copy ceqn->solution_'s values into p_data_ (GridField)
  for (int i = 0; i < ceqn->p_grid_->n_nodes(); i++) {
    for (int j = 0; j < ceqn->n_dof(); j++) {
      if (j == 0) {
        ceqn->p_data_->GetNodeData(i).phi[0] =
            ceqn->solution_(i * ceqn->n_dof() + j);
      } else {
        ceqn->p_data_->GetNodeData(i).ion[j - 1] =
            ceqn->solution_(i * ceqn->n_dof() + j);
      }
    }

    // ceqn->p_data_->GetNodeData (i).value (3) = 0.001;
  }
  return ierr;
}

/***
 * Computes the global matrix and vector.
 */
PetscErrorCode FormFunction(SNES snes, Vec _xg, Vec _f, void *ceqn_) {
  PetscErrorCode ier;
  PNPEquation *ceqn = (PNPEquation *)ceqn_;
  ceqn->counter++;

  // copy guess into the GridField so we can use its values
  // when calculating residual
  timers[kTimerUpdate].Start();
  UpdateGridField(_xg, ceqn);
  timers[kTimerUpdate].Stop();

  // the normal solve step (set dirichlet, assemble, apply dirichlet)
  ceqn->fillEssBC();
  timers[kTimerAssemble].Start();
  ceqn->Assemble(false);
  ceqn->ApplyEssBC();

  // Assemble() fills ceqn->bg_, but we need to store the result in f
  ier = VecCopy(ceqn->bg(), _f);
  CHKERRQ(ier);
  return 0;
}

/***
 * The Jacobian is already calculated by FormFunction's Assemble() call.
 * We do both at once so we can reuse the basis function values at each element
 * (which take a significant amount of time to calculate).
 * So, we do nothing here.
 */
PetscErrorCode FormJacobian(SNES snes, Vec _xg, Mat jac, Mat B, void *ceqn_) {
  return 0;
}
};
#endif
