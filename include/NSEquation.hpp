#pragma once

#include <talyfem/talyfem.h>
#include "deviceInputData.hpp"
#include "deviceGridField.hpp"
#include "deviceNodeData.hpp"
#include <math.h>

class NSEquation : public CEquation<DDNodeData> {
 public:
  /// pointer to input_data
  DDInputData *input_data;
  /// nonlinear solver context
  SNES snes;
  KSP ksp;
  /// number of function calls
  int counter;

  NSEquation(DDInputData *input_data);  /// Define fillEssBC
  void fillEssBC();

  /// Define Solve
  void Solve(double t, double dt);

  /// Define  Integrands
  void Integrands(const FEMElm &fe, ZeroMatrix<double> &Ae,
                  ZEROARRAY<double> &be);
  /**
  void Integrands4side(const FEMElm &fe, int sideInd, ZeroMatrix<double> &Ae,
                       ZEROARRAY<double> &be);
  */
  /// probably the flux
  double p_val;
};

PetscErrorCode UpdateGridField(Vec _xg, NSEquation *ceqn, int flag = 0);

PetscErrorCode FormFunctionNS(SNES snes, Vec x, Vec f, void *ceqn);

PetscErrorCode FormJacobianNS(SNES snes, Vec _xg, Mat jac, Mat B, void *ceqn_);

NSEquation::NSEquation(DDInputData *input_data) : input_data(input_data) {
  counter = 0;
  p_val = 0;
}

void NSEquation::fillEssBC() {
  this->initEssBC();
  double eps = 1e-6;
  for (int nodeID = 0; nodeID < p_grid_->n_nodes(); nodeID++) {
    DDNodeData &data = p_data_->GetNodeData(nodeID);
    for (std::vector<int>::iterator it = input_data->inletBC.begin();
         it != input_data->inletBC.end(); ++it) {
      if (this->p_grid_->BoNode(nodeID, *it)) {
        for (int j = 0; j < p_grid_->nsd(); j++) {
          this->specifyValue(nodeID, j, 0.0);
        }
        data.u[0] = input_data->InletUx / input_data->U0();
        data.u[1] = input_data->InletUy / input_data->U0();
      }
    }

    for (std::vector<int>::iterator it = input_data->outletBC.begin();
         it != input_data->outletBC.end(); ++it) {
      if (this->p_grid_->BoNode(nodeID, *it)) {
        this->specifyValue(nodeID, p_grid_->nsd(), 0.0);
        data.PRESSURE = 0.0;
      }
    }

    for (std::vector<int>::iterator it = input_data->wallBC.begin();
         it != input_data->wallBC.end(); ++it) {
      if (this->p_grid_->BoNode(nodeID, *it)) {
        for (int j = 0; j < p_grid_->nsd(); j++) {
          this->specifyValue(nodeID, j, 0.0);
        }
        data.u[0] = 0;
        data.u[1] = 0;
      }
    }
  }
}

void NSEquation::Solve(double t, double dt) {
  UpdateMatPreallocation();

  this->t_ = t;
  this->dt_ = dt;
  /// Assemble the Jacobian matrix and Function vector
  this->fillEssBC();
  // this->Assemble(1);
  // this->ApplyEssBC();
  /// Create nonlinear solver context
  PetscErrorCode ierr;
  ierr = SNESCreate(PETSC_COMM_WORLD, &snes);
  CHKERRV(ierr);

  /// Set initial Guess while checking if mesh partitioning is used i.e.
  /// nParallelType==2
  if (this->p_grid_->parallel_type_ == kWithDomainDecomp) {
    /// PETSc functions
    PetscScalar *array;
    ierr = PetscMalloc(this->n_total_dof() * sizeof(PetscScalar), &array);
    CHKERRV(ierr);
    Vec initVec;
    VecScatter scatter;
    ierr = VecCreateSeqWithArray(PETSC_COMM_SELF, 1, this->n_total_dof(), array,
                                 &initVec);
    CHKERRV(ierr);

    /// Copy the node data from NSGridField into the array backing initVec
    for (int nodeID = 0; nodeID < this->p_grid_->n_nodes(); nodeID++) {
      /// Set velocity from NodeData
      /// pressure is not copied here as pressure index is not always
      DDNodeData &data = this->p_data_->GetNodeData(nodeID);
      for (int i = 0; i < this->input_data->nsd; i++) {
        array[n_dof() * nodeID + i] = data.u[i];
      }
      /// Set pressure because now pressure has a constant index
      array[n_dof() * nodeID + input_data->nsd] = data.PRESSURE;
    }

    //
    ierr = VecScatterCreate(initVec, this->to(), this->xg_, this->from(),
                            &scatter);
    CHKERRV(ierr);
    ierr = VecScatterBegin(scatter, initVec, this->xg_, INSERT_VALUES,
                           SCATTER_FORWARD);
    CHKERRV(ierr);
    ierr = VecScatterEnd(scatter, initVec, this->xg_, INSERT_VALUES,
                         SCATTER_FORWARD);
    CHKERRV(ierr);

    ierr = VecDestroy(&initVec);
    CHKERRV(ierr);

    ierr = VecScatterDestroy(&scatter);
    CHKERRV(ierr);

    ierr = PetscFree(array);
    CHKERRV(ierr);
  } else {
    /// non-DD case
    std::vector<double> val(n_dof());
    std::vector<PetscInt> index(n_dof());
    for (int nodeID = 0; nodeID < this->p_grid_->n_nodes(); nodeID++) {
      /// setting initial guess
      DDNodeData &data = p_data_->GetNodeData(nodeID);

      /// Copy velocity
      for (int i = 0; i < this->input_data->nsd; i++) {
        val[i] = data.u[i];
        index[i] = n_dof() * nodeID + i;
      }

      /// Copy pressure
      val[input_data->nsd] = data.PRESSURE;
      index[input_data->nsd] = n_dof() * nodeID + input_data->nsd;

      ierr = VecSetValues(this->xg_, this->n_dof_, index.data(), val.data(),
                          INSERT_VALUES);
      CHKERRV(ierr);
    }
    ierr = VecAssemblyBegin(this->xg_);
    CHKERRV(ierr);
    ierr = VecAssemblyEnd(this->xg_);
    CHKERRV(ierr);
  }
  // MatView(this->Ag_,PETSC_VIEWER_STDOUT_SELF);

  // Set function evaluation routine and vector.
  ierr = SNESSetFunction(snes, this->bg_, FormFunctionNS, this);
  CHKERRV(ierr);
  // Set jacobian matrix
  ierr = SNESSetJacobian(snes, this->Ag_, this->Ag_, FormJacobianNS, this);
  CHKERRV(ierr);

  double atol_ksp = 1e-8, rtol_ksp = 1e-8, dtol_ksp = 100;
  int maxit_ksp = 1500;

  // counter = 0;  ///< Resetting function counter
  ierr = SNESGetKSP(snes, &ksp);
  CHKERRV(ierr);
  KSPSetTolerances(ksp, rtol_ksp, atol_ksp, dtol_ksp, maxit_ksp);

  double atol_snes = 1e-10, rtol_snes = 1e-10, stol_snes = 1e-10;
  int maxit_snes = 50, maxf = 1000;
  SNESSetTolerances(snes, atol_snes, rtol_snes, stol_snes, maxit_snes, maxf);
  SNESMonitorSet(snes, SNESMonitorDefault, PETSC_NULL, PETSC_NULL);
  SNESSetFromOptions(snes);
  ierr = SNESSolve(snes, PETSC_NULL, this->xg_);
  CHKERRV(ierr);
  // Get number of iterations used
  // PetscInt its;
  // SNESConvergedReason reason;
  // SNESGetIterationNumber(snes,&its);
  // SNESGetConvergedReason( snes, &reason );
  // PetscPrintf(PETSC_COMM_WORLD,"number of iterations = %d\t%d\n",its,reason);

  // Store solution to pData
  UpdateGridField(this->xg_, this);

  ierr = SNESDestroy(&snes);
  CHKERRV(ierr);
}

/*
void NSEquation::Integrands4side(const FEMElm &fe, int sideInd,
                                 ZeroMatrix<double> &Ae,
                                 ZEROARRAY<double> &be) {
  if (sideInd == 1) {
    double Re = this->input_data->Re;
    const int nsd = input_data->nsd;
    const int nbf = fe.nbf();
    const double detJxW = fe.detJxW();
    double Coe_diff;

    if (Re > 1) {
      Coe_diff = 1.0 / Re;
    } else if (Re <= 1) {
      Coe_diff = 1.0;
    }

    /// field variables
    ZEROPTV u;
    for (int i = 0; i < nsd; i++) {
      u(i) = this->p_data_->valueFEM(fe, i);
    }

    ZeroMatrix<double> du;
    du.redim(nsd, nsd);
    for (int i = 0; i < nsd; i++) {
      for (int j = 0; j < nsd; j++) {
        du(i, j) = this->p_data_->valueDerivativeFEM(fe, i, j);
      }
    }

    for (int a = 0; a < nbf; a++) {
      for (int b = 0; b < nbf; b++) {

        for (int i = 0; i < nsd; i++) {
          double diff_J = 0;
          for (int j = 0; j < nsd; j++) {
            diff_J +=
                -Coe_diff * fe.N(a) * fe.dN(b, j) * (fe.surface()->normal()(j));
          }

          Ae((nsd + 1) * a + i, (nsd + 1) * b + i) += diff_J * detJxW;
          for (int j = 0; j < nsd; j++) {
            Ae((nsd + 1) * a + i, (nsd + 1) * b + j) +=
                -Coe_diff * fe.N(a) * fe.dN(b, i) *
                (fe.surface()->normal()(j)) * detJxW;
          }
        }
      }
      ZEROPTV diff;
      for (int i = 0; i < nsd; i++) {
        diff(i) = 0.0;
        for (int j = 0; j < nsd; j++)
          diff(i) += -Coe_diff * fe.N(a) *
                     (du(i, j) * (fe.surface()->normal()(j)) +
                      du(j, i) * (fe.surface()->normal()(j))) *
                     detJxW;
      }

      for (int i = 0; i < nsd; i++) {
        be((nsd + 1) * a + i) +=
            diff(i) + fe.N(a) * p_val * (fe.surface()->normal()(i)) * detJxW;
      }
    }
  }
}
*/
/** Assemble Ae and Be matrix for incompressible Navier-Stokes equation,
 * The PDE is:
 * rho * (du/dt + u . grad{u}) + grad{p} - mu * grad . (grad{u} + grad{u}^T) = 0
 * grad . {u}
 * The nondimensional form of the PDE is:
 * (du/dt + u . grad{u}) + grad{p} - (1 / Re) * grad . (grad{u} + grad{u}^T) = 0
 * grad . {delta_u} = 0
 * Linearize the equation will yield:
 * LHS = Ae = [ d delta_u/dt + delta_u . grad{u} + u . grad{delta_u} +
 * grad{delta_p} - (1 / Re) * grad. (grad{delta_u} + grad{delta_u}^T) ] *
 * delta_u
 * RHS = Be = (du/dt + u . grad{u}) + grad{p} - (1 / Re) * grad . (grad{u} +
 * grad{u}^T)
 * */
void NSEquation::Integrands(const FEMElm &fe, ZeroMatrix<double> &Ae,
                            ZEROARRAY<double> &be) {
  int nsd = this->input_data->nsd;  ///< No. of dimensions
  double ion = 0;
  for (int speciesID = 0; speciesID < input_data->num_species; speciesID++) {
    ion += this->p_data_->valueFEM(fe, ion_id[speciesID]) *
           input_data->valence[speciesID];
  }

  double phi = this->p_data_->valueFEM(fe, phi_id);
  ZEROPTV E;
  for (int i = 0; i < nsd; i++) {
    E(i) = this->p_data_->valueDerivativeFEM(fe, phi_id, i);
  }
  double St = this->input_data->St;
  double Re = this->input_data->Re;  ///< Get Reynolds number from input_data
  double Coe_diff;  ///< Coefficient to diffusion term in Residual of NS
  double Coe_conv;  ///< Coefficient to advection term in Residual of NS
  /** This helps when Re is extremely small (viscosity dominant), being in the
   * denominator can
   * make the solution blowup, this ensures that the matrix is well behaved
   */
  if (Re > 1) {
    Coe_diff = 1.0 / Re;
    Coe_conv = 1.0;
  } else if (Re <= 1) {
    Coe_diff = 1.0;
    Coe_conv = Re;
  }
  ///----Calculate resuduals for stabilizer terms in be (right side)---------///
  ///----Get necessary parameters from the inpPRESSURE data and solution (for
  ///non-linaer solver)----////

  int nbf = fe.nbf();           ///<No of basis functions
  double detJxW = fe.detJxW();  ///<Mapping from isoparametric space

  /** Get u and u_pre from the NodeData
   * NOTE: value is defined on NodeData object, therefore it follows
   * indices defined in NodeData subclass.
   * NOTE: Every iteration the solution is copied into NodeData, we use these
   * solved fields from the NodeData object to calculate the be (NS resudual)
   * of current iteration.
   */
  ZEROPTV u, u_pre;
  for (int i = 0; i < nsd; i++) {
    u(i) = this->p_data_->valueFEM(fe, ux_id + i);
    u_pre(i) = this->p_data_->valueFEM(fe, ux_pre_id + i);
  }
  /// Define velocity gradient tensor ( grad{u} )
  ZeroMatrix<double> du;
  du.redim(nsd, nsd);
  for (int i = 0; i < nsd; i++) {
    for (int j = 0; j < nsd; j++) {
      du(i, j) = this->p_data_->valueDerivativeFEM(fe, ux_id + i, j);
    }
  }
  /// This seems incorrect
  /*
  ZeroMatrix<double> d2u;
  d2u.redim(nsd, nsd);
  for (int i = 0; i < nsd; i++) {
    for (int j = 0; j < nsd; j++) {
      // d2u(i,j) = this->p_data_->valPRESSURE2DerivativeFEM(fe,i,j);
      // coPRESSURE<<d2u(i,j)<<endl;
    }
  }
  */

  /** Remember your defined on the value is on NodeData object, so use
   * indices the way you defined in NodeData, we acquire pressure from the
   * NodeData.
   */
  double p = p_data_->valueFEM(fe, PRESSURE_id);

  /// Get gradient of pressure
  ZEROPTV dp;
  for (int i = 0; i < nsd; i++) {
    dp(i) = this->p_data_->valueDerivativeFEM(fe, PRESSURE_id, i);
    // since the body force due to either electric field (or gravity) are all
    // vectors (compariable to pressure gradient),
    // I just add them to the pressure gradient;
    dp(i) += ion / input_data->Avogadro * this->input_data->prefactorElecBodyF *
             this->p_data_->valueDerivativeFEM(fe, phi_id, i);
  }
  /** We define the convection of Navier Stokes here from
   * using inner product of gradient tensor and fields we acquired above.
   *
   * We don't calculate Diffusion here as the contribution of diffusion to
   * SUPG terms is zero and we use this to form the NS(i) resudual.
   */

  /// convec = u . grad{u} (advection term in RHS)
  ZEROPTV convec;
  ZEROPTV diffusion;
  for (int i = 0; i < nsd; i++) {
    convec(i) = 0.0;
    diffusion(i) = 0;
    for (int j = 0; j < nsd; j++) {
      convec(i) += du(i, j) * u(j);
      // diffusion(i) += Coe_diff*d2u(i,j);
    }
  }

  /** Construct the Navier Stokes equation withoPRESSURE diffusion
   * Here diffusion is not present as its contribution to stabilizers for
   * linear basis functions is zero
   */
  /// NS = Coe_conv(du/dt + u . grad{u}) + grad{p}
  ZEROPTV NS;
  for (int i = 0; i < nsd; i++) {
    NS(i) = Coe_conv * (u(i) - u_pre(i)) / dt_ + Coe_conv * convec(i) +
            dp(i);  // - diffusion(i);
  }
  /** Calculate continuity resudual for PSPG stabilizer
   * This resudual is a essential for calculating the PSPG stabilizer
   */
  double cont = 0.0;
  for (int i = 0; i < nsd; i++) cont += du(i, i);

  ///----------------------Done with resuduals for stabilizers---------------///

  /** Now we define SUPG and PSPG stabilizer terms
   * these terms require Navier-Stokes (NS(i)) resudual for SUPG and
   * continuity (cont)
   * resudual for PSPG
   */
  /** U is global scaling velocity
   */
  ZEROPTV U;

  U(0) = 1.0;
  U(1) = 0.0;
  U(2) = 0.0;

  TezduyarUpwindFE PG;

  PG.calcSUPGWeightingFunction(fe, u, Coe_diff);
  PG.calcPSPGWeightingFunction(fe, U, 1.0, Coe_diff);

  /** Calculating the Elemental matrices requires loop over basis functions
   * We loop over basis functions and calculate the Jacobian and the Residual
   * The equation we are solving is \vect{J} \cdot \delta u = -E
   * Here \vect{J} is the Jacobian matrix operating on \delta u and E is the
   * resudual.  We will be using stabilized forms of Jacobian matrix and
   * resudual
   */
  for (int a = 0; a < nbf; a++) {
    for (int b = 0; b < nbf; b++) {
      ///---------------Calculating the Jacobian operator--------------------///
      /** Linearize the equation will yield:
     * LHS = Ae = [ d delta_u/dt + delta_u . grad{u} + u . grad{delta_u} +
     * grad{delta_p} - (1 / Re) * grad. (grad{delta_u} + grad{delta_u}^T) ] *
     * delta_u
     * RHS = Be = (du/dt + u . grad{u}) + grad{p} - (1 / Re) * grad . (grad{u} +
     * grad{u}^T)
     */
      /// All terms withoPRESSURE stabilization
      /// conv = u . grad{delta_u}
      double conv = 0.0;
      for (int i = 0; i < nsd; i++) {
        conv += u(i) * fe.dN(b, i);
      }
      ///<this is incorrect
      /*
      double diff_NS = 0.0;
      for (int i = 0; i < nsd; i++) {
        // diff_NS += Coe_diff*fe.d2N(b,i);
      }
      */

      /// Diffusion term_1 at LHS
      /// diff_J = -grad . (grad{u}) = (grad{w}, grad{u})
      for (int i = 0; i < nsd; i++) {
        double diff_J = 0;
        for (int j = 0; j < nsd; j++) {
          diff_J += Coe_diff * fe.dN(a, j) * fe.dN(b, j);
        }

        /** This calculates (w, d{delta_u}/dt) + (w, u_n . grad{delta_u}) +
         * (grad{w}, grad{u})
         * We calculated u_n . grad{delta_u} as "conv" and (grad{w}, grad{u}) as
         * diff_J
         * then when multiplied with fe.dN for the weighting function
         * These terms go in the diagonal of the matrix
         */
        Ae((nsd + 1) * a + i, (nsd + 1) * b + i) +=
            (Coe_conv * fe.N(a) * (fe.N(b) / dt_ + conv) + diff_J) * detJxW;

        /** This loop calculates (delta_u . grad{u}) - grad . (grad{u}^T)
         * In weak form, they are (w, grad{u}) + (grad{w}, grad{u}^T)
         * */
        for (int j = 0; j < nsd; j++) {
          Ae((nsd + 1) * a + i, (nsd + 1) * b + j) +=
              /// This term calculates (w, (delta_u . grad{u}))
              Coe_conv * fe.N(a) * du(i, j) * fe.N(b) * detJxW
              /// This term calculates (grad{w}, grad{delta_u}^T)
              + Coe_diff * fe.dN(a, j) * fe.dN(b, i) * detJxW;
        }

        /** This term calculates contribution of pressure to Jacobian matrix
         * mathematically the term is:  -(grad{w}, delta_p)
         */
        Ae((nsd + 1) * a + i, (nsd + 1) * b + nsd) +=
            -fe.dN(a, i) * fe.N(b) * detJxW;
      }

      /// Terms with stabilization

      /// SUPG
      for (int i = 0; i < nsd; i++) {
        /**Stabilizer terms to first and second term of Navier Stokes
         * Doesn't need to be inside the nsd loop as that has been done
         * in calculation of "conv"
         * Again these terms contribPRESSUREe to the diagonal of the matrix
         */
        Ae((nsd + 1) * a + i, (nsd + 1) * b + i) +=
            Coe_conv * PG.SUPG(a) * (fe.N(b) / dt_ + conv /*+diff_NS*/) *
            detJxW;
        /** ContribPRESSUREion of SUPG term of convection
         * (tau_M u_n .grad{w}, NS_resudual(no diffusion as that would be zero))
         * The PG.SUPG is like fe.N, it's an additional weighting function
         * This term calculates
         */
        for (int j = 0; j < nsd; j++) {
          /** Notice diffusion not added here in contrast with the analog of
           * normal terms. Here we calculate the SUPG complimentary term of
           * the 2nd convection term (w, (delta_u . grad{u_n}) given by
           * (SUPG, (delta_u . grad{u_n}), here we weight with SUPG function
           * instead.
           */
          Ae((nsd + 1) * a + i, (nsd + 1) * b + j) +=
              Coe_conv * PG.SUPG(a) * du(i, j) * fe.N(b) * detJxW;
        }

        /** This is SUPG analog of the pressure term given by
         * -(div{SUPG},delta_p)
         * Again this gets added to the last diagonal term of the matrix
         */
        Ae((nsd + 1) * a + i, (nsd + 1) * b + nsd) +=
            PG.SUPG(a) * fe.dN(b, i) * detJxW;
      }

      /// PSPG
      /** We now use the PSPG stabilisation for continuity and pressure.
       * Note: PSPG stabilisation still uses Navier Stokes as resudual,
       * therefore we will be multiplying the PSPG weighting funciton with
       * Navier-Stokes resudual (withoPRESSURE diffusion), we will add this to
       * continuity equation
       */
      for (int i = 0; i < nsd; i++) {
        Ae((nsd + 1) * a + nsd, (nsd + 1) * b + i) +=
            fe.N(a) * fe.dN(b, i) * detJxW +
            Coe_conv * PG.PSPG(a, i) * (fe.N(b) / dt_ + conv /*+diff_NS*/) *
            detJxW;

        for (int j = 0; j < nsd; j++) {
          Ae((nsd + 1) * a + nsd, (nsd + 1) * b + i) +=
              Coe_conv * PG.PSPG(a, j) * du(j, i) * fe.N(b) * detJxW;
        }

        Ae((nsd + 1) * a + nsd, (nsd + 1) * b + nsd) +=
            PG.PSPG(a, i) * fe.dN(b, i) * detJxW;
      }
    }
    ///------------Assembly of Jacobian operator complete--------------------///

    ///------------Calculating the right hand side (resudual)----------------///
    /** Calculate the weakened Navier-Stokes resudual at the current iteration
     * We calculate all the terms inclPRESSUREing their SUPG and PSPG analogs,
     * SUPG will not take diffusion term
     */

    ZEROPTV NS_stable_terms;
    ZEROPTV normal_NS;
    ZEROPTV NS_contrib;  ///< stabilizer;

    /** Calculating diffusion term of Navier-Stokes at current iteration.
     * This is done using the stress form of the diffusion term given by
     * (Coe_diff* div{(grad{u_n}+T{grad{u_n}})}), here T{} is the transpose
     * operator.
     */
    ZEROPTV diff;
    for (int i = 0; i < nsd; i++) {
      diff(i) = 0.0;
      for (int j = 0; j < nsd; j++) {
        diff(i) += Coe_diff * fe.dN(a, j) * (du(i, j) + du(j, i));
      }
    }
    /** Here we assemble all the terms of weakened Navier-Stokes*/
    for (int i = 0; i < nsd; i++) {
      normal_NS(i) = Coe_conv * fe.N(a) * (u(i) - u_pre(i)) / dt_ * detJxW +
                     Coe_conv * fe.N(a) * (convec(i)) * detJxW +
                     (-fe.dN(a, i) * p) * detJxW + (diff(i)) * detJxW;
      /** Calculate the SUPG analogs, remember we use the diffusionless
       * resudual calculated in the first part of integrands given by
       * NS(i) = Coe_conv * (u(i) - u_pre(i)) / dt_ + Coe_conv * convec(i) +
       *     dp(i); // - diffusion(i);
       */
      NS_stable_terms(i) = (PG.SUPG(a) * NS(i)) * detJxW;

      /** Here we add the terms of Navier-Stokes with Stabilizer terms and
       * call them NS_contrib, Navier-Stokes contribution to the be vector
       */
      NS_contrib(i) = normal_NS(i) + NS_stable_terms(i);
    }

    /** Now we have to add the contribution of pressure to the be vector.
     * This naturally will be the last term of the vector
     */

    ///< We first calculate the PSPG stabilisation to the pressure term, which
    ///< still takes the NS(i) resudual
    double Cont_stable_term = 0.0;
    for (int i = 0; i < nsd; i++) {
      Cont_stable_term += PG.PSPG(a, i) * NS(i);
    }

    /// Add contribution of normal continuity term and stabilisation term
    double Cont_contrib =
        (fe.N(a) * (cont) * detJxW) + (Cont_stable_term * detJxW);

    /// Assembling the terms of be vector, first rows are contributions from
    /// Navier Stokes and last term is the contribution from continuity
    for (int i = 0; i < nsd; i++) {
      be((nsd + 1) * a + i) += NS_contrib(i);
      be((nsd + 1) * a + nsd) += Cont_contrib;
    }
  }
}

PetscErrorCode UpdateGridField(Vec _xg, NSEquation *ceqn, int flag) {
  PetscErrorCode ierr;

  /// collect solution from 'xg' and store into 'solution'
  if (ceqn->p_grid_->parallel_type_ == kWithDomainDecomp) {
    VecScatter scatter;
    Vec SolPRESSUREionVec;
    ierr = VecCreateSeqWithArray(PETSC_COMM_SELF, 1, ceqn->n_total_dof(),
                                 ceqn->solution_.data(), &SolPRESSUREionVec);
    CHKERRQ(ierr);
    ierr = VecScatterCreate(_xg, ceqn->from(), SolPRESSUREionVec, ceqn->to(),
                            &scatter);
    CHKERRQ(ierr);
    ierr = VecScatterBegin(scatter, _xg, SolPRESSUREionVec, INSERT_VALUES,
                           SCATTER_FORWARD);
    CHKERRQ(ierr);
    ierr = VecScatterEnd(scatter, _xg, SolPRESSUREionVec, INSERT_VALUES,
                         SCATTER_FORWARD);
    CHKERRQ(ierr);

    ierr = VecDestroy(&SolPRESSUREionVec);
    CHKERRQ(ierr);

    ierr = VecScatterDestroy(&scatter);
    CHKERRQ(ierr);

  } else {
    VecScatter scatter;
    Vec solutionVec;
    ierr = VecScatterCreateToAll(_xg, &scatter, &solutionVec);
    CHKERRQ(ierr);
    ierr = VecScatterBegin(scatter, _xg, solutionVec, INSERT_VALUES,
                           SCATTER_FORWARD);
    CHKERRQ(ierr);
    ierr = VecScatterEnd(scatter, _xg, solutionVec, INSERT_VALUES,
                         SCATTER_FORWARD);
    CHKERRQ(ierr);
    double *array;
    ierr = VecGetArray(solutionVec, &array);
    CHKERRQ(ierr);
    memcpy(ceqn->solution_.data(), array,
           sizeof(double) * (ceqn->n_total_dof()));
    ierr = VecRestoreArray(solutionVec, &array);
    CHKERRQ(ierr);

    ierr = VecScatterDestroy(&scatter);
    CHKERRQ(ierr);

    ierr = VecDestroy(&solutionVec);
    CHKERRQ(ierr);
  }

  /** 2D solution vector: [ vx vy p | vx vy p | vx vy p | ... ]
  * 3D solution vector: [ vx vy vz p | vx vy vz p | vx vy vz p | ... ]
  * there are (nsd + 1) variables for each node in the solution vector **/
  int vars_per_node_in_sol = (ceqn->input_data->nsd + 1);
  ///< Copy ceqn->solution to the pData structure
  for (int i = 0; i < ceqn->p_grid_->n_nodes(); i++) {
    for (int j = 0; j < ceqn->input_data->nsd; j++) {
      ceqn->p_data_->GetNodeData(i).u[j] =
          ceqn->solution_(vars_per_node_in_sol * i + j);
    }
    ceqn->p_data_->GetNodeData(i).PRESSURE =
        ceqn->solution_(vars_per_node_in_sol * i + ceqn->input_data->nsd);
  }

  return ierr;
}

/// EvalPRESSUREte the right hand side which is the resudual for the nonlinear
/// solver
PetscErrorCode FormFunctionNS(SNES snes, Vec _xg, Vec _f, void *ceqn_) {
  PetscErrorCode ier;
  NSEquation *ceqn = (NSEquation *) ceqn_;
  ceqn->counter++;

  //< CompPRESSUREes Ag and bg
  UpdateGridField(_xg, ceqn);
  ceqn->fillEssBC();
  ceqn->Assemble();
  ceqn->ApplyEssBC();

  ier = VecCopy(ceqn->bg(), _f);
  CHKERRQ(ier);
  return 0;
}

PetscErrorCode FormJacobianNS(SNES snes, Vec _xg, Mat jac, Mat B, void *ceqn_) {
  return 0;
}
