/*
  Copyright 2016-2017 Baskar Ganapathysubramanian

  This file is part of TALYFem.

  TALYFem is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  TALYFem is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with TALYFem.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef DD_INPUT_DATA_HPP
#define DD_INPUT_DATA_HPP

using namespace std;

#include <map>
#include <string>
#include <talyfem/talyfem.h>

struct DDInputData : public InputData {
  double InletUx = 0, InletUy = 0, InletUz = 0, InletU = 1e-5;   //inlet velocisyt m/s
  double density = 1e3; //density of water 1e3 kg/m3
  double eta = 1.e-3;  //absolute/dynamic viscosity of water ( Pa s )
  double dypressure = density * InletU * InletU; //dynamic pressure;
  double dt = 2e-2;
  double theta = 0.5;
  double currenttime = 0, endtime = 1;
  double Re = 1., St = 1.;
  double x1, x2, x3, x4, y2;
  vector<int> inletBC, outletBC, potentialBC, groundBC, wallBC, constC_BC;
  vector<int> valence;
  vector<double> initIonC;
  vector<string> ionName;
  int num_species, num_inletBC, num_outletBC, num_potentialBC, num_groundBC, num_wallBC, num_constC_BC;
  int ifPrintPltFiles;          ///< whether to print .plt files at start and end
  int shouldFail;
  int use_elemental_assembly_;  ///< 1 if elemental assembly is to be used
  // Universal constants
  double Avogadro = 6.02e23;
  double epsilon_0 = 8.854e-12; //< Permittivity of free space      --As m^-1 V^-1
  double k_B = 1.3806503e-23;   //< Boltzmann constant        --m^2 kg s^(-2) K^(-1)
  double T = 300;               //< Room temperature        --K
  double q = 1.60217646e-19;    //< Elementary charge       --As,coulombs
  double Vt = k_B * T / q;
  double Faraday = 96485.3329;   // Faraday constant         --s A/mol
  double Lx = 1, Ly = 1;
  // Parameters, in M.K.S
  // Material Parameters 
  double init_c = 6.02e23;          //< initial particle density of position and negative particles  -- m^(-3)
  double init_c_factor = 1.0;       //initial concentration  --mol m^(-3)
  double voltage = 1.1;             //applied voltage or the voltage difference at top and bottom      --eV
  double mu_anion = 1e-7;           //< Electron mobility when in majority          --m^2/(Vs)
  double mu_cation = 1e-7;          //< Hole mobility when in majority              --m^2/(Vs)
  double G;                     //< Generation rate         --m^(-3)/s                   
  double Height;                //< Domain length                       --m 
  double dy;     //minimum length scale (element size) in y direction; 
  double eps_electrolyte = 3 * 8.854e-12;
  double eps_DNA = 3 * 8.854e-12;
  double interfaceThk = 2.;
  int isPeriodic = 0;
  // Structured Mesh parameters
  int Nelemx = 0;                //< Number of elements in x-direction
  int Nelemy = 0;                //< Number of elements in y-direction
  int Nelemz = 0;                //< Number of elements in z-direction
  double beta_1 = 0;                 //< Clustering parameter
  // Calculated parameters for 1-D case
  double Voc;
  // MPI parameters
  int mpi_size;
  int mpi_rank;
  //prefactor of electric body force (comes from the non-dimensionalization of NS equation)
  double prefactorElecBodyF;
  //The velocity in DD equation and in NS equation are non-dimensionalized in different way.
  //This factor is used as the prefactor of velocity in DD equation;
  double prefactorOfU = 0;
  // other parameters
  char **args;

  // Morphology parameter
  double midphi;
  double chi;


  // Input ms file name
  std::string MSfile;

  // Input mesh file if _readmesh_ is defined
  char *meshFile;
  // Input ms file [tecplot] node number in x-dir and y-dir
  int msDataNx, msDataNy, msDataNz;

  // Exciton generation rate profile
  // 1-constant 2-exponential 3-reflecting cathode
  int Gprofile;
  //----------------------------------------------------------------------------------------------

  double insulatingdomain;      //the size of insulating domain around high-k particles.


  double V_t() {
    return Vt;
  }                             // Thermal voltage [V] [V]=0.0258520269


  // Scaling parameters [ref: S. Selberherr, 1984, page 141]
  //----------------------------------------------------------------------------------------------
  double t0() {
    return this->Height * this->Height / mu0() / this->Vt;
  }

  double x0() {
    return this->Height;
  }

  double V0() {
    return this->Vt;
  }

  double C0() {
    return this->init_c;
  }

  double mu0() {
    return (this->mu_anion > this->mu_cation ? this->mu_anion : this->mu_cation);
  }

  double U0() {
    return InletU;
  }

  // Perturbation parameter
  double lambda2_(double epsilong) {
    double eps;
    eps = epsilong;
    double returnVal = V0() * eps / (x0() * x0() * this->q * C0());
    return returnVal;
  }                             // Perturbation parameter

  // Scaled variables
  //----------------------------------------------------------------------------------------------        
  double x_hat(double x) {
    return x / x0();
  }

  double V_hat(double V) {
    return V / V0();
  }

  double c_hat(double c) {
    return c / C0();
  }

  double mu_hat(double mu) {
    return mu / mu0();
  }

  double U_hat(double U) {
    return U / U0();
  }

  double P_hat(double P) {
    return P / dypressure;
  }

  // Unscaled variables
  //----------------------------------------------------------------------------------------------
  double x_unhat(double x_hat) {
    return x_hat * x0();
  }

  double V_unhat(double V_hat) {
    return V_hat * V0();
  }

  double c_unhat(double c_hat) {
    return c_hat * C0();
  }

  double mu_unhat(double mu_hat) {
    return mu_hat * mu0();
  }

  double U_unhat(double U_hat) {
    return U_hat * U0();
  }

  double P_unhat(double P_hat) {
    return P_hat * dypressure;
  }


  DDInputData() : InputData(), ifPrintPltFiles(1), shouldFail(0), use_elemental_assembly_(0) {
  }

  bool ReadFromFile(const std::string &filename = std::string("config.txt")) {
    // Read config file and initialize basic fields 
    InputData::ReadFromFile(filename);

    if (ReadValue("ifPrintPltFiles", ifPrintPltFiles)) {
    }
    if (ReadValue("shouldFail", shouldFail)) {
    }
    if (ReadValue("use_elemental_assembly", use_elemental_assembly_)) {
    }
    if (ReadValue("mu_anion", mu_anion)) {
    }
    if (ReadValue("mu_cation", mu_cation)) {
    }
    if (ReadValue("voltage", voltage)) {
    }
    if (ifBoxGrid) {
      if (ReadValue("Nelemx", Nelemx)) {
      }
      if (ReadValue("Nelemy", Nelemy)) {
      }
      if (ReadValue("Lx", L[0])) {
      }
      if (ReadValue("Ly", L[1])) {
      }
    }

    if (ReadValue("num_constC_BC", num_constC_BC)) {}
    if (ReadValue("num_wallBC", num_wallBC)) {
    }
    if (ReadValue("num_inletBC", num_inletBC)) {
    }
    if (ReadValue("num_outletBC", num_outletBC)) {
    }
    if (ReadValue("num_potentialBC", num_potentialBC)) {
    }
    if (ReadValue("num_groundBC", num_groundBC)) {
    }
    if (ReadArray("wallBC", wallBC, num_wallBC)) {
    }
    if (ReadArray("inletBC", inletBC, num_inletBC)) {
    }
    if (ReadArray("outletBC", outletBC, num_outletBC)) {
    }
    if (ReadArray("constC_BC", constC_BC, num_constC_BC)) {
    }

    if (ReadArray("potentialBC", potentialBC, num_potentialBC)) {
    }
    if (ReadArray("groundBC", groundBC, num_groundBC)) {
    }
    if (ReadValue("num_species", num_species)) {
    }
    if (ReadArray("ionName", ionName, num_species)) {
    }
    if (ReadArray("valence", valence, num_species)) {
    }
    if (ReadArray("initIonC", initIonC, num_species)) {
      for (int i = 0; i < num_species; i++) {
        initIonC[i] *= init_c;
      }
    }
    if (ReadValue("Density", density)) {
    }
    if (ReadValue("Viscosity", eta)) {
    }
    if (ReadValue("Nelemz", Nelemz)) {
    }
    if (ReadValue("Height", Height)) {
    }
    if (ReadValue("eps_electrolyte", eps_electrolyte)) {
      eps_electrolyte *= 8.854e-12;
    }
    if (ReadValue("eps_DNA", eps_DNA)) {
      eps_DNA *= 8.854e-12;
    }
    if (ReadValue("isPeriodic", isPeriodic)) {
    }
    if (ReadValue("init_concentration", init_c_factor)) {
      init_c *= init_c_factor;
    }

    if (ReadValue("beta", beta_1)) {
    }

    if (ReadValue("T", T)) {                           //then update thermal volrage with new temperature.
      this->Vt = k_B * T / q;
    }
    if (ReadValue("dy", dy)) {
    }
    if (ReadValue("endtime", endtime)) {
    }
    if (ReadValue("theta", theta)) {
    }
    if (ReadValue("InletUx", InletUx)) {
    }
    if (ReadValue("InletUy", InletUy)) {
      InletU = sqrt(InletUx * InletUx + InletUy * InletUy);
      Re = density * InletU * Height / eta;
      St = t0() * U0() / x0();
      dypressure = density * InletU * InletU;
    }
    prefactorElecBodyF = Vt * Faraday * init_c_factor / density / x0() / U0() / U0();
    prefactorOfU = x0() / V0() / mu0();
    return true;
  }

  //CFL condition to determin dt, time step
  void CFL() {  //return a non-dimensionalized time step, dt
    double saftyfactor = 2;
    // CFL coming from DD equaiton
    double dt1 = dy / (mu0() * voltage / Height);  //dimentional dt;
    dt1 = dt1 / t0();
    // CFL coming from NS equation
    double dt2 = 1e-6;
    // use the minimum of the two    
    //dt = dt1>dt2?dt2:dt1;

    //dt /= saftyfactor;
  }

  // sinh(a x)/(1+2*b*sinh(a/2 x)*sinh(a/2 x))
  double RHSpotential(double a, double b, double x) {
    x *= V0();
    x -= voltage / 2.;
    return sinh(a * x) / (1 + 2 * b * sinh(a / 2. * x) * sinh(a / 2. * x));
  }

  // derivative of sinh(a x)/(1+2*b*sinh(a/2 x)*sinh(a/2 x))
  double derivativeRHSpotential(double a, double b, double x) {
    x *= V0();
    x -= voltage / 2.;
    double temp = 2. * b * sinh(a * x / 2) * sinh(a * x / 2.) + 1.;
    return -(a * cosh(a * x) / temp - 2 * a * b * sinh(a * x / 2.) * sinh(a * x) * cosh(a * x / 2) / temp / temp);
  }


  bool CheckInputData() const {
    if (((typeOfIC == 0) && (inputFilenameGridField == "")) || ((typeOfIC != 0) && (inputFilenameGridField != ""))) {
      PrintWarning("IC not set properly check!", typeOfIC, " ", inputFilenameGridField);
      return false;
    }

    return InputData::CheckInputData();
  }

  std::ostream &print(std::ostream &oss) const {
    PrintLogStream(oss, "ifPrintPltFiles = ", ifPrintPltFiles);
    PrintLogStream(oss, "shouldFail = ", shouldFail);
    return InputData::print(oss);
  }

};

#endif
