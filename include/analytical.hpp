#include "talyfem/talyfem.h"

//the functions for analytical solution.
double CpSolAt(double t, double x, double y) {
  double sol_;

  sol_ = cos(2. * M_PI * t) * cos(x) * sin(y);
  return sol_;
}

double CnSolAt(double t, double x, double y) {
  double sol_;

  sol_ = -cos(2. * M_PI * t) * sin(x) * cos(y);
  return sol_;
}

double PhiSolAt(double t, double x, double y) {

  double sol_;

  sol_ = cos(4. * M_PI * t) * cos(x) * sin(y);
  return sol_;

}

double CpForceAt(double t, double x, double y, double invPe) {
  double xForcing;
  xForcing = 2. * invPe * cos(2. * M_PI * t) * cos(x) * sin(y) - 2. * M_PI * sin(2. * M_PI * t) * cos(x) * sin(y)
             - invPe * (cos(2. * M_PI * t) * cos(4. * M_PI * t) * sin(x) * sin(x) * sin(y) * sin(y)
                        + cos(2. * M_PI * t) * cos(4. * M_PI * t) * cos(x) * cos(x) * cos(y) * cos(y))
             - cos(2. * M_PI * t) * cos(2. * M_PI * t) * cos(x) * cos(x) * cos(y) * sin(y)
             - cos(2. * M_PI * t) * cos(2. * M_PI * t) * cos(y) * sin(x) * sin(x) * sin(y)
             + 2. * invPe * cos(2. * M_PI * t) * cos(4. * M_PI * t) * cos(x) * cos(x) * sin(y) * sin(y);
  return xForcing;
}


double CnForceAt(double t, double x, double y, double invPe) {
  double yForcing;
  yForcing = 2. * M_PI * sin(2. * M_PI * t) * cos(y) * sin(x) - 2. * invPe * cos(2. * M_PI * t) * cos(y) * sin(x)
             - cos(2. * M_PI * t) * cos(2. * M_PI * t) * cos(x) * cos(y) * cos(y) * sin(x)
             - cos(2. * M_PI * t) * cos(2. * M_PI * t) * cos(x) * sin(x) * sin(y) * sin(y)
             + 4. * invPe * cos(2. * M_PI * t) * cos(4. * M_PI * t) * cos(x) * cos(y) * sin(x) * sin(y);
  return yForcing;
}

double PhiForceAt(double t, double x, double y) {
  double lamda = 1., L = 1.;
  double force;
  force = (4. * L * L * cos(4. * M_PI * t) * cos(x) * sin(y)) / (lamda * lamda)
          - cos(2. * M_PI * t) * cos(y) * sin(x) - cos(2. * M_PI * t) * cos(x) * sin(y);
  return force;
}
