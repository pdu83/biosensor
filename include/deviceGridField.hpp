/*
  Copyright 2016-2017 Baskar Ganapathysubramanian

  This file is part of TALYFem.

  TALYFem is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  TALYFem is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with TALYFem.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef BT_GRIDFIELD_HPP
#define BT_GRIDFIELD_HPP

#include "analytical.hpp"
#include <iostream>
#include <fstream>
#include "deviceNodeData.hpp"
#include "deviceInputData.hpp"
#include <vector>

using std::vector;
using namespace std;
typedef vector<double> vec;
typedef vector<vec> Mat2d;
typedef vector<vector<vec>> Mat3d;

using namespace std;

class DDGridField : public GridField<DDNodeData> {
 public:
  DDInputData *input_;

  DDGridField(DDInputData *input) : input_(input) {}

  virtual ~DDGridField() {}

  /**
   * set the initial condition:
   * 1. coentration and potential from time step 0 and 1
   * 2. mobility of each species and the dielectric constant of the electrolyte.
   */
  void SetIC(int nsd) {
    double mu_anion = input_->mu_anion;
    double mu_cation = input_->mu_cation;

    for (int nodeID = 0; nodeID < p_grid_->n_nodes(); nodeID++) {
      DDNodeData *pData = &(GetNodeData(nodeID));
      double x = p_grid_->GetCoord(nodeID, 0);
      double y = p_grid_->GetCoord(nodeID, 1);
      pData->phi[0] =
          (1. - abs(x - 25.5) / 25.) * input_->voltage / input_->V0();
      for (int species = 0; species < input_->num_species; species++) {
        pData->ion[species] = input_->c_hat(input_->initIonC[species]);
        pData->old_ion[species] = pData->ion[species];
      }
      // manufactured solution at time=0 as initial condition;
      pData->phi[0] = PhiSolAt(0, x, y);
      pData->ion[1] = CnSolAt(0, x, y);
      pData->ion[0] = CpSolAt(0, x, y);
      pData->old_ion[1] = CnSolAt(0, x, y);
      pData->old_ion[0] = CpSolAt(0, x, y);
      pData->mun = mu_anion;
      pData->mup = mu_cation;
      pData->epsn = input_->eps_electrolyte;
    }

    PrintStatusStream(std::cerr, "IC set ");
  }

  /**
   * compute and stores new values of jn and jp by doing post process.
   * generally in this method first we need to find the node that Jn and Jp
   * start to give bad results
   * After finding that point we will do 4th order forward and backward finite
   * difference to subsitute Jn and Jp values
   * At the end of the function values of grad of jn and grad of Jp are computed
   * as well to test accuracy of results
   *
   * TODO break out parts of this function that are very similar into smaller,
   * reusable functions
   * a single function should not span 250+ lines without good reason
   * I think your comments here are pretty good though
   */
  void post_process(bool if_domain_decomp, const DDInputData *input_) {
    FEMElm fe(p_grid_, BASIS_FIRST_DERIVATIVE | BASIS_POSITION);
    double delta_jnx = 0;
    double delta_jpx = 0;
    int accuracy_order = 4;
    int n_tot = (input_->Nelem[0] + 1) * (input_->Nelem[1] + 1);
    int nx = (input_->Nelem[0] + 1);
    double hx = input_->Height / input_->Nelem[0];
    double hy = (input_->nsd >= 2) ? input_->Height / input_->Nelem[1] : 0.0;
    int jn_point = 0;
    int jp_point = 0;
    /***
    Finding the node that Jn starts to have bad results
    */
    for (int nodeID = p_grid_->n_nodes() - 1; nodeID >= 0; nodeID--) {
      DDNodeData *pData = &(GetNodeData(nodeID));
      if (nodeID <= 1 * nx || nodeID > n_tot - 2 * nx) continue;
      DDNodeData *pData1 = &(GetNodeData(nodeID + nx));  // data of above node
      DDNodeData *pData2 = &(GetNodeData(nodeID + 1));   // data of right node
      double y_1 = p_grid_->GetCoord(nodeID, 1);
      double delta_1 = fabs((pData1->j[0] - pData->j[0]) / hy +
                            (pData2->j[1] - pData->j[1]) / hx);
      if (delta_1 > 20 &&
          y_1 < 0.2)  // if grad of Jp is a large number (not reasonable)
      {
        jn_point = nodeID;
        std::cout << " At y = " << y_1 << " and node_id = " << nodeID
                  << " Jp post process started" << std::endl;
        break;
      }
    }
    /***
    Post process of Jn. From the node we found in previous step we do 4th
    Forward difference to subsitute bad results
    */
    for (int nodeID = jn_point; nodeID >= 0; nodeID--) {
      DDNodeData *pData = &(GetNodeData(nodeID));
      int nex = (input_->Nelem[0]);
      int ney = (input_->Nelem[1]);
      float factor = 0;
      if (nex < ney) {
        factor = float(nex) / ney;
      } else {
        factor = float(ney) / nex;
      }
      factor *= 0.1;
    }
    /***
    Finding the node that Jp starts to have bad results
    */
    for (int nodeID = 0; nodeID < p_grid_->n_nodes(); nodeID++) {
      DDNodeData *pData = &(GetNodeData(nodeID));

      if (nodeID > n_tot - 2 * nx) continue;
      DDNodeData *pData1 = &(GetNodeData(nodeID + nx));  // data of above node
      DDNodeData *pData2 = &(GetNodeData(nodeID + 1));   // data of right node
      double y_1 = p_grid_->GetCoord(nodeID, 1);
      double delta_1 = fabs((pData1->j[2] - pData->j[2]) / hy +
                            (pData2->j[3] - pData->j[3]) / hx);
      if (delta_1 > 20 &&
          y_1 > 0.8)  // if grad of Jp is a large number (not reasonable)
      {
        jp_point = nodeID;
        std::cout << " At y = " << y_1 << " and node_id = " << nodeID
                  << " Jp post process started" << std::endl;
        break;
      }
    }
    /***
    Post process of Jp. From the node we found in previous step we do 4th
    backward difference to subsitute bad results
    */
    for (int nodeID = jp_point; nodeID < n_tot; nodeID++) {
      DDNodeData *pData = &(GetNodeData(nodeID));
      int nex = (input_->Nelem[0]);
      int ney = (input_->Nelem[1]);
      float factor = 0;
      if (nex < ney) {
        factor = float(nex) / ney;
      } else {
        factor = float(ney) / nex;
      }
      factor *= 0.1;
    }
    for (int nodeID = 0; nodeID < p_grid_->n_nodes(); nodeID++) {
      DDNodeData *pData = &(GetNodeData(nodeID));
      if (pData->j[4] < 0) pData->j[4] *= -1;
      if (pData->j[5] < 0) pData->j[5] *= -1;
    }
  }

  /***
  * Computing Jc, output current density.
  * @return value: current density of Jn, Jp and J in a vector form;
  */
  std::vector<float> integrateJ(DDGridField &data) {
    int j, e;
    int nel = p_grid_->n_elements();

    int boundary_bottom = 3;
    int boundary_top = 4;
    int flag, flag_bottom;

    double bfarea = 0, tparea = 0;  // for area integration

    double Jbot = 0;
    double Jtop = 0;

    // loop over all the elements
    for (e = 0; e < nel; e++) {
      // initialize flags
      flag = 0;
      flag_bottom = 0;
      // STEP1:  Check if this element lies on either boundary
      // find the number of nodes in this element
      int nne = this->p_grid_->GetElm(e)->n_nodes();
      // Check to see if this node lies on boundary
      for (j = 0; j < nne; j++) {
        // Check to see if the element is a boundary element
        int nodeID = this->p_grid_->GetLocalNodeID(e, j);
        if (this->p_grid_->BoNode(nodeID, boundary_bottom)) {
          flag = 1;
          flag_bottom = 1;
          break;
        } else if (this->p_grid_->BoNode(nodeID, boundary_top)) {
          flag = 1;
          break;
        }
      }
      // STEP 2: Find the line integral
      //-----------------------------------------------------
      if (flag) {
        ELEM *pElm;
        pElm = this->p_grid_->GetElm(e);
        FEMElm fe(p_grid_, BASIS_ALL);
        // set the indicator
        int interfaceInd = (flag_bottom > 0) ? boundary_bottom : boundary_top;
        // perform surface/line integration
        // loop over all the surfaces in the element
        for (ELEM::SurfaceList_type::iterator it =
            pElm->surface_indicator_.begin();
             it != pElm->surface_indicator_.end(); it++) {
          double area = 0;
          // check to see if this surface is the correct surface
          // cout << "indicators: " << it->indicators() << endl;
          if (it->has_indicator(interfaceInd)) {
            // loop over the surface gauss points
            fe.refill_surface(e, &*it, BASIS_LINEAR, 0);
            while (fe.next_itg_pt()) {
              double detJxW = fe.detJxW();
              double Jny = 0;
              double Jpy = 0;
              if (interfaceInd == boundary_bottom)
                Jbot += Jny * detJxW;
              else
                Jtop += Jpy * detJxW;
              area += detJxW;
            }
            if (flag_bottom) {
              bfarea += area;
            } else {
              tparea += area;
            }
          }
        }
      }
    }
    // send current density integral to proc 0
    double rJtop;
    double rJbot;
    MPI_Allreduce(&Jtop, &rJtop, 1, MPI_DOUBLE, MPI_SUM, PETSC_COMM_WORLD);
    MPI_Allreduce(&Jbot, &rJbot, 1, MPI_DOUBLE, MPI_SUM, PETSC_COMM_WORLD);
    // send area to proc 0
    double rtop;
    double rbot;
    MPI_Allreduce(&tparea, &rtop, 1, MPI_DOUBLE, MPI_SUM, PETSC_COMM_WORLD);
    MPI_Allreduce(&bfarea, &rbot, 1, MPI_DOUBLE, MPI_SUM, PETSC_COMM_WORLD);
    tparea = rtop;
    bfarea = rbot;
    rJtop /= tparea;
    rJbot /= bfarea;
    std::vector<float> J;
    J.push_back(-rJtop);
    J.push_back(-rJbot);
    rJtop > rJbot ? J.push_back(-rJbot) : J.push_back(-rJtop);
    return J;
  }

  // Cluster the mesh toward electrods,
  void meshclustering(const DDInputData *input_) {
    // Grid clustering
    double y, y_new, eta, x_hat = 1.;
    double beta = 1 + input_->beta_1;
    for (int nodeID = 0; nodeID < p_grid_->n_nodes(); nodeID++) {
      y = p_grid_->GetCoord(nodeID, 1);
      eta = y / x_hat;
      y_new = x_hat * ((1 + beta) * pow((beta + 1) / (beta - 1), 2 * eta - 1) +
                       1 - beta) /
              (2 * (1 + pow((beta + 1) / (beta - 1), 2 * eta - 1)));
      p_grid_->node_array_[nodeID]->setCoor(1, y_new);
    }
    printf("finished Interpolate initialize...\n");
  }

  /***
  * Computing Jc, output current density.
  *
  */
  std::vector<float> FindJc(bool if_domain_decomp, const DDInputData *input_) {
    std::vector<float> J;
    int nx = (input_->Nelemx + 1);
    int n_tot = p_grid_->n_nodes();
    float jc1 = 0;
    float jc2 = 0;
    for (int nodeID = 0; nodeID < nx; nodeID++) {
      DDNodeData *pData = &(GetNodeData(nodeID));
      jc1 += pData->j[0];
    }
    for (int nodeID = n_tot - 1; nodeID >= n_tot - nx; nodeID--) {
      DDNodeData *pData = &(GetNodeData(nodeID));
      jc2 += pData->j[2];
    }
    J.push_back(-jc1 / nx);
    J.push_back(-jc2 / nx);
    J.push_back((jc1 < jc2 ? -jc1 : -jc2) / nx);
    return J;
  }

  /*  void SetIndicators (DDInputData & inputData)
    {
      if(input_->domainShape==0){
      double halfThick = inputData.y2;
      double tol = 0.000001;
      double left1 = inputData.x1, right1 = inputData.x4;
      double  left2=inputData.x2, right2 = inputData.x3;
      for (int nodeID = 0; nodeID < p_grid_->n_nodes (); nodeID++)
      {
        NodeIndicator indicators = 0;
        double x = p_grid_->GetCoord (nodeID, 0);
        double y = p_grid_->GetCoord (nodeID, 1);

        if (fabs (x) < left1+tol && fabs(y)>=halfThick)
        {
          indicators |= INDICATOR_NUM (1);
        }
        else if (fabs (x) <left2+tol && fabs(y)<halfThick)
        {
          indicators |= INDICATOR_NUM (2);
        }
        else if (fabs (x) > right1-tol  && fabs(y)>=halfThick)
        {
          indicators |= INDICATOR_NUM (5);
        }
        else if (fabs (x) > right2-tol && fabs(y)<halfThick )
        {
          indicators |= INDICATOR_NUM (6);
        }
        else if (fabs(y)>=halfThick-tol && fabs(y)<=halfThick+tol && fabs
    (x)<left2)
        {
          indicators |= INDICATOR_NUM (7);
        }
        else if (fabs(y)>=halfThick-tol && fabs(y)<=halfThick+tol && fabs
    (x)>right2)
        {
          indicators |= INDICATOR_NUM (8);
        }
        if (fabs (y) < tol)
        {
          indicators |= INDICATOR_NUM (3);
        }
        if (fabs (y - 1.0) < tol)
        {
          indicators |= INDICATOR_NUM (4);
        }
        p_grid_->GetNode (nodeID)->setIndicators (indicators);
      }
      p_grid_->cared_surface_indicator_.appendData (1);
      p_grid_->cared_surface_indicator_.appendData (2);
      p_grid_->cared_surface_indicator_.appendData (3);
      p_grid_->cared_surface_indicator_.appendData (4);
      p_grid_->cared_surface_indicator_.appendData (5);
      p_grid_->cared_surface_indicator_.appendData (6);
      p_grid_->cared_surface_indicator_.appendData (7);
      p_grid_->cared_surface_indicator_.appendData (8);
      p_grid_->GenElmSurfaceIndicator ();
     }else{
       double  right1 = inputData.x4;
      double tol = 0.000001;
      for (int nodeID = 0; nodeID < p_grid_->n_nodes (); nodeID++)
      {
        NodeIndicator indicators = 0;
        double x = p_grid_->GetCoord (nodeID, 0);
        double y = p_grid_->GetCoord (nodeID, 1);

        if (fabs (y) < tol)
        {
          indicators |= INDICATOR_NUM (1);
        }
        else if(fabs (x) < tol) {
          indicators |= INDICATOR_NUM (2);
        }
        else if(fabs (x) >right1- tol) {
          indicators |= INDICATOR_NUM (3);
        }
        p_grid_->GetNode (nodeID)->setIndicators (indicators);
      }
      p_grid_->cared_surface_indicator_.appendData (1);
      p_grid_->cared_surface_indicator_.appendData (2);
      p_grid_->cared_surface_indicator_.appendData (3);
      p_grid_->GenElmSurfaceIndicator ();

     }
    }

  */

  /***
   Convert non-dimentional varaible to dimentional form;
  */
  void dimlize() {
    PrintStatus(
        "This function is applied only the the primary unknows, cation,anion, "
            "potential, velocity & pressure.");
    for (int nodeID = 0; nodeID < p_grid_->n_nodes(); nodeID++) {
      DDNodeData *pData = &(GetNodeData(nodeID));
      for (int speciesID = 0; speciesID < input_->num_species; speciesID++)
        pData->ion[speciesID] =
            input_->c_unhat(pData->ion[speciesID]) / input_->Avogadro;  // ions
      pData->phi[0] = input_->V_unhat(pData->phi[0]);  // potential
      pData->u[3] = input_->U_unhat(pData->u[3]);
      pData->u[4] = input_->U_unhat(pData->u[4]);
      pData->u[5] = input_->U_unhat(pData->u[5]);
      pData->PRESSURE = input_->P_unhat(pData->PRESSURE);
    }
  }

  /***
   Convert dimentional varaible to non-dimentional form;
  */
  void nondimlize() {
    for (int nodeID = 0; nodeID < p_grid_->n_nodes(); nodeID++) {
      DDNodeData *pData = &(GetNodeData(nodeID));
      for (int speciesID = 0; speciesID < input_->num_species; speciesID++)
        pData->ion[speciesID] =
            input_->c_hat(pData->ion[speciesID]) * input_->Avogadro;
      pData->phi[0] = input_->V_hat(pData->phi[0]);
      pData->u[3] = input_->U_hat(pData->u[3]);
      pData->u[4] = input_->U_hat(pData->u[4]);
      pData->u[5] = input_->U_hat(pData->u[5]);
      pData->PRESSURE = input_->P_hat(pData->PRESSURE);
    }
  }

  /***
   * update unknowns between two time steps
   */
  void update(double currenttime) {
    for (int nodeID = 0; nodeID < p_grid_->n_nodes(); nodeID++) {
      DDNodeData *pData = &(GetNodeData(nodeID));
      for (int speciesID = 0; speciesID < input_->num_species; speciesID++)
        pData->old_ion[speciesID] = pData->ion[speciesID];
      pData->old_u[0] = pData->u[0];
      pData->old_u[1] = pData->u[1];
      pData->old_u[2] = pData->u[2];

      // only for analytical solution
      double x = p_grid_->GetCoord(nodeID, 0);
      double y = p_grid_->GetCoord(nodeID, 1);
      pData->u[1] = CnSolAt(currenttime, x, y);
      pData->u[0] = CpSolAt(currenttime, x, y);
    }
  }

  /***
   * The function calculate relative error between two arrays.
   * ID1, ID2 are two index of variables from DDNodeData;
   * if ID2 > 0: calculate the relative error between two arrays, ie,
   * relative difference between two time steps of a certain variable;
   * if ID2 ==-1: calculate the relative error between two block iterations;
   */
  double relError(int ID1, int ID2) {
    int n_tot = p_grid_->n_nodes();
    double relDiff = 0;
    double absValue = 1e-20;
    if (ID2 >= 0) {
      for (int nodeID = 0; nodeID < n_tot; nodeID++) {
        DDNodeData *pData = &(GetNodeData(nodeID));
        relDiff += pData->value(ID1) - pData->value(ID2);
      }
    } else {
    }
    return relDiff / absValue;
  }
};

#endif
