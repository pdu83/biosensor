/*
  Copyright 2014-2015 Baskar Ganapathysubramanian

  This file is part of TALYFem.

  TALYFem is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  TALYFem is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with TALYFem.  If not, see <http://www.gnu.org/licenses/>.
*/
// --- end license text --- //
#ifndef DD_NODEDATA_HPP
#define DD_NODEDATA_HPP

#include "deviceInputData.hpp"

// Global variables define a meaningful name for the index.
const int num_cation = 1;
const int num_anion = 1;
const int muanion_id = 39;
const int mucation_id = 29;
const int eps_ID = 49;
const int n_coupled = 3;
int nsd = 2;
static int phi_id = 7;
static int ion_id[10] = {11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
static int old_ion_id[10] = {21, 22, 23, 24, 25, 26, 27, 28, 29, 30};
int PRESSURE_id = 0;
int ux_pre_id = 4;
int uy_pre_id = 5;
int uz_pre_id = 6;
int ux_id = 1;
int uy_id = 2;
int uz_id = 3;

class DDNodeData {
 public:
  static int nsd;
  double phi[2];
  double ion[8];
  double old_ion[8];
  double PRESSURE;
  double u[3], old_u[3];
  double j[8];
  double epsn;
  double mun;
  double mup;

  // global variable
  static string speciesName[8];

  static void setSpecies(DDInputData &input) {
    for (int i = 0; i < input.num_species; i++) {
      speciesName[i] = input.ionName[i];
    }
  }

  double &value(int index) {
    // first 1+nsd*2 stores pressure and velocity

    if (index == 0) {
      return PRESSURE;
    } else if (index == 1) {
      return u[0];
    } else if (index == 2) {
      return u[1];
    } else if (index == 3) {
      return u[2];
    } else if (index == 4) {
      return old_u[0];
    } else if (index == 5) {
      return old_u[1];
    } else if (index == 6) {
      return old_u[2];
    } else if (index == phi_id) {
      return phi[0];
    } else if (index > 10 && index < 19) {
      return ion[index - 11];
    } else if (index > 20 && index < 29) {
      return old_ion[index - 21];
    } else if (index == 49) {
      return epsn;
    } else if (index == 39) {
      return mun;
    } else if (index == 29) {
      return mup;
    } else {
      throw TALYException() << "Invalid DDNodeData index";
    }
  }

  const double &value(int index) const {
    return const_cast<DDNodeData *>(this)->value(index);
  }

  static char *name(int index) {
    static char str[256];

    if (index == 0) {
      snprintf(str, 256, "pressure");
    } else if (index == 1) {
      snprintf(str, 256, "ux");
    } else if (index == 2) {
      snprintf(str, 256, "uy");
    } else if (index == 3) {
      snprintf(str, 256, "uz");
    } else if (index == 4) {
      snprintf(str, 256, "ux_old");
    } else if (index == 5) {
      snprintf(str, 256, "uy_old");
    } else if (index == 6) {
      snprintf(str, 256, "uz_old");
    } else if (index == phi_id) {
      snprintf(str, 256, "phi");
    } else if (index > 10 && index < 19) {
      snprintf(str, 256, "%s", speciesName[index - 11].c_str());
    } else {
      str[0] = '\0';
    }

    return str;
  }

  static int valueno() { return 50; }
};

// if you get an error about multiple definitions, move this into a .cpp file
string DDNodeData::speciesName[8];

#endif
